// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic','ionic.service.core', 'starter.controllers', 'ngCordova','services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
  $ionicConfigProvider.platform.android.navBar.alignTitle('center');
  $ionicConfigProvider.platform.ios.backButton.previousTitleText('Atras').icon('ion-ios-arrow-thin-left');
  $ionicConfigProvider.platform.android.backButton.previousTitleText('Atras').icon('ion-android-arrow-back');
  $ionicConfigProvider.platform.android.tabs.position('bottom');

  $stateProvider

  .state('home', {
    url: '/home',
    templateUrl: 'templates/home.html',
    controller: 'HomeCtrl'
  })
  .state('turismo', {
    url: '/turismo',
    templateUrl: 'templates/turismo.html',
    controller: 'TurismoCtrl'
  })
  .state('perfil', {
    url: '/perfil',
    templateUrl: 'templates/perfil.html',
    controller: 'PerfilCtrl'
  })
  .state('filterornot', {
    url: '/filterornot/:categriaId',
    templateUrl: 'templates/filterornot.html',
    controller: 'FilterornotCtrl'
  })
  .state('filter', {
    url: '/filter/:categriaId',
    templateUrl: 'templates/filter.html',
    controller: 'FilterCtrl'
  })
  .state('nofilter', {
    url: '/nofilter/:categriaId',
    templateUrl: 'templates/nofilter.html',
    controller: 'NofilterCtrl'
  })
  .state('mapa', {
    url: '/mapa',
    templateUrl: 'templates/mapa.html',
    controller: 'MapaCtrl'
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/home');
});
