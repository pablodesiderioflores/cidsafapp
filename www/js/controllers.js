angular.module('starter.controllers', [])

.controller('HomeCtrl', function($scope, $state, $q, UserService, $ionicLoading) {
	 // This is the success callback from the login method
  var fbLoginSuccess = function(response) {
    if (!response.authResponse){
      fbLoginError("Cannot find the authResponse");
      return;
    }

    var authResponse = response.authResponse;

    getFacebookProfileInfo(authResponse)
    .then(function(profileInfo) {
      // For the purpose of this example I will store user data on local storage
      UserService.setUser({
        authResponse: authResponse,
        userID: profileInfo.id,
        name: profileInfo.name,
        email: profileInfo.email,
        picture : "http://graph.facebook.com/" + authResponse.userID + "/picture?type=large"
      });
      $ionicLoading.hide();
      $state.go('perfil');
    }, function(fail){
      // Fail get profile info
      console.log('profile info fail', fail);
    });
  };

  // This is the fail callback from the login method
  var fbLoginError = function(error){
    console.log('fbLoginError', error);
    $ionicLoading.hide();
  };

  // This method is to get the user profile info from the facebook api
  var getFacebookProfileInfo = function (authResponse) {
    var info = $q.defer();

    facebookConnectPlugin.api('/me?fields=email,name&access_token=' + authResponse.accessToken, null,
      function (response) {
        console.log(response);
        info.resolve(response);
      },
      function (response) {
        console.log(response);
        info.reject(response);
      }
    );
    return info.promise;
  };

  //This method is executed when the user press the "Login with facebook" button
  $scope.facebookSignIn = function() {
    facebookConnectPlugin.getLoginStatus(function(success){
      if(success.status === 'connected'){
        // The user is logged in and has authenticated your app, and response.authResponse supplies
        // the user's ID, a valid access token, a signed request, and the time the access token
        // and signed request each expire
        console.log('getLoginStatus', success.status);

        // Check if we have our user saved
        var user = UserService.getUser('facebook');

        if(!user.userID){
          getFacebookProfileInfo(success.authResponse)
          .then(function(profileInfo) {
            // For the purpose of this example I will store user data on local storage
            UserService.setUser({
              authResponse: success.authResponse,
              userID: profileInfo.id,
              name: profileInfo.name,
              email: profileInfo.email,
              picture : "http://graph.facebook.com/" + success.authResponse.userID + "/picture?type=large"
            });

            $state.go('perfil');
          }, function(fail){
            // Fail get profile info
            console.log('profile info fail', fail);
          });
        }else{
          $state.go('perfil');
        }
      } else {
        // If (success.status === 'not_authorized') the user is logged in to Facebook,
        // but has not authenticated your app
        // Else the person is not logged into Facebook,
        // so we're not sure if they are logged into this app or not.

        console.log('getLoginStatus', success.status);

        $ionicLoading.show({
          template: 'Logueando...'
        });

        // Ask the permissions you need. You can learn more about
        // FB permissions here: https://developers.facebook.com/docs/facebook-login/permissions/v2.4
        facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
      }
    });
  };
})

.controller('PerfilCtrl', function($scope, UserService, $ionicActionSheet, $state, $ionicLoading, $http) {
  $scope.user = UserService.getUser();

  $scope.showLogOutMenu = function() {
    var hideSheet = $ionicActionSheet.show({
      destructiveText: 'Salir',
      titleText: '¿Estás seguro que quieres irte? Esta aplicación es asombrosa quedate un rato más.',
      cancelText: 'Cancelar',
      cancel: function() {},
      buttonClicked: function(index) {
        return true;
      },
      destructiveButtonClicked: function(){
        $ionicLoading.show({
          template: 'Saliendo...'
        });

        // Facebook logout
        facebookConnectPlugin.logout(function(){
          $ionicLoading.hide();
          $state.go('home');
        },
        function(fail){
          $ionicLoading.hide();
        });
      }
    });
  };

  var link = "http://pabloflores.com.ar/cidsafrest/getCategorias.php";
  $ionicLoading.show({
        content: 'Loading',
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 50,
        showDelay: 0
  });

  $http.post(link).then(function(res) {
      /*Set a timeout to clear loader, however you would
      actually call the $ionicLoading.hide();
      method whenever everything is ready or loaded.*/
      $ionicLoading.hide();
      $scope.categorias = res.data;
      console.log(res.data);
    }, function errorCallback(response) {
         $ionicLoading.hide();
         alert('Error al conectarse al servidor, intentelo de nuevo.\n Si el problema persiste comuniquese con el administrador ');
  });
})

.controller('TurismoCtrl', function($scope) {
	
})

.controller('MapaCtrl', function($scope) {
	
})
.controller('NofilterCtrl', function($scope, $stateParams, $ionicLoading, $http) {
  var id = $stateParams.categriaId;
  var link  = "http://pabloflores.com.ar/cidsafrest/getSociosNoFilter.php?datos="+id;
  var datos = id;
  $ionicLoading.show({
        content: 'Loading',
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 50,
        showDelay: 0
  });

  $http.post(link).then(function(res) {
      /*Set a timeout to clear loader, however you would
      actually call the $ionicLoading.hide();
      method whenever everything is ready or loaded.*/
      $ionicLoading.hide();
      $scope.socios = res.data;
      console.log(res.data);
    }, function errorCallback(response) {
         $ionicLoading.hide();
         alert('Error al conectarse al servidor, intentelo de nuevo.\n Si el problema persiste comuniquese con el administrador ');
  });
})

.controller('FilterornotCtrl', function($scope, $stateParams) {
  $scope.categoria = $stateParams;
});